/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import _ from 'lodash';
import {InsertOneWriteOpResult} from 'mongodb';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {CategoryDao} from '../dao/CategoryDao';
import {ProcessDao} from '../dao/ProcessDao';

import {BaseController} from './BaseController';

export class ProcessController extends BaseController {

    processDao: ProcessDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);
        this.processDao = new ProcessDao(this.db);
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /process/add İşlem Ekle
     * @apiName ProcessAdd
     * @apiGroup Process
     * @apiDescription Yeni İşlem
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı.
     *
     * @apiParam {String} categoryId Kategori ID'si
     * @apiParam {String} title İşlem Başlığı
     * @apiParam {String} processKey İşlem Anahtarı
     * @apiParam {String=sql, file, request} [systemType] Sistem Tipi
     * @apiParam {Date} [expiredTime] Süre
     * @apiParam {Date} [hidden] Gizli Sorgu.
     * @apiParam {String} [dbKey] Veritabanı Anahtarı
     * @apiParam {String} [icon] İşlem İconu
     * @apiParam {Json} [presentParams] MpAnalysis hızlı erişim kodları
     * @apiParam {Json} [charts] Grafik Ayarları
     * @apiParam {Json} [queryItems] Sorgular
     * @apiParam {Json} [events] Sorgu Olayları
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiErrorExample {json} error Hata
     *     HTTP/1.1 4xxx error
     *     {
     *          error: DATABASE_CONNECTION_ERROR
     *          error: SET_USER_ADMIN_COMPANY_NOT_BE_SAVED
     *          error: COMPANY_NOT_BE_SAVED
     *          error: INSERT_ERROR
     *     }
     *
     * @apiSampleRequest /process/add
     */
    public async add() {

        this.requireParameters(['categoryId', 'title', 'processKey']);

        const process = this.getProcess();

        const result: InsertOneWriteOpResult<any> = await this.processDao.insert(process);

        if (result.insertedCount === 1) {
            this.response.send({success: 'OK', insertedId: result.insertedId});
        } else {
            throw new ErrorModel('PROCESS_NOT_BE_SAVED', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /process/delete İşlem Sil
     * @apiName ProcessDelete
     * @apiGroup Process
     * @apiDescription İşlem Sil
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı.
     *
     * @apiParam {String} processId İşlem ID'si
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiErrorExample {json} error Hata
     *     HTTP/1.1 4xxx error
     *     {
     *          error: DATABASE_CONNECTION_ERROR
     *          error: SET_USER_ADMIN_COMPANY_NOT_BE_SAVED
     *          error: COMPANY_NOT_BE_SAVED
     *          error: INSERT_ERROR
     *     }
     *
     * @apiSampleRequest /process/delete
     */
    public async delete() {

        this.requireParameters(['processId']);

        const processId = this.getParam('id|processId');
        const result = await this.processDao.deleteWithId(processId);

        if (result && result.deletedCount && result.deletedCount > 0) {

            this.response.send({success: 'OK'});
        } else {

            throw new ErrorModel('PROCESS_NOT_BE_DELETED', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /process/list İşlem Listesi
     * @apiName ProcessList
     * @apiGroup Process
     * @apiDescription İşlem Listesi
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} [userId=LogonUserId] Kullanıcı ID'si
     * @apiParam {String} [categoryId] Kategori ID'si
     * @apiParam {String} [copy=false] True gelirse sistemdeki tüm sorgular listelenir.
     * @apiParam {Boolean} [hidden=false] True gelirse gizli process objelerini de listeye dahil eder.
     * @apiParam {Boolean} [queryItems=false] True gelirse process objesine queryItems ekler.
     * @apiParam {Boolean} [mpAnalysis=false] True gelirse process objesine presentParams ve charts ekler.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "items": []
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /process/list
     */
    public async list() {

        const userId = this.getParam('id|userId') || this.user._id;
        const categoryId = this.getParam('id|categoryId') || null;
        const copy = this.getParam('copy');

        const hidden = this.getParam('hidden') || false;
        const queryItems = this.getParam('queryItems') || false;
        const mpAnalysis = this.getParam('mpAnalysis') || false;

        const categoryDao = new CategoryDao(this.db);

        const categoryIds: any[] = !!categoryId
            ? [categoryId]
            : await categoryDao.getIdList(userId);

        const filter: any = copy
            ? {}
            : {
                categoryId: {
                    $in: categoryIds
                },
            };

        if (!hidden) {
            filter.hidden = false;
        }

        const shown: any = {};

        if (queryItems) {
            shown.queryItems = 1;
        }

        // mobil uygulamadan mpAnalysis=true olarak gönderildiğinde present params ve charts eklenir.
        if (mpAnalysis) {
            shown.presentParams = 1;
            shown.charts = 1;
        }

        const result = await this.processDao.getListItems([{$match: filter}], shown);

        this.response.send({items: result});
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /process/get İşlem Objesi
     * @apiName ProcessGet
     * @apiGroup Process
     * @apiDescription İşlem Al
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} [userId=logonUserId] User ID, root,admin,machine yetkilerinden birini gerektirir.
     * @apiParam {String} [_id] İşlem ID `processId` kullanılıyorsa zorunlu değil
     * @apiParam {String} [processId] İşlem ID `_id` kullanılıyorsa zorunlu değil
     * @apiParam {String} title İşlem Anahtarı
     * @apiParam {String} processKey İşlem Anahtarı
     * @apiParam {String} dbKey Db Key
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "item": {}
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /process/get
     */
    public async get() {

        this.requireParameters([
            ['_id', 'processKey', 'processId'],
        ]);

        const userId = this.getRoledParam(['root', 'admin', 'machine'], 'id|userId') || this.user._id;

        if (_.isNil(userId)) {
            throw new ErrorModel('AUTH_USER_IS_UNDEFINED', 200);
        }

        const values = this.setValues()
            .setValuesWithParams([
                'id|processId|_id',
                'id|categoryId',
                'title',
                'processKey',
                'expiredTime',
                'hidden',
                'dbKey',
            ])
            .getValues();

        const categoryDao = new CategoryDao(this.db);

        const categoryIds: any[] = await categoryDao.getIdList(userId, false);

        const filter = {categoryId: {$in: categoryIds}, ...values};
        const result = await this.processDao.getItems([{$match: filter}]);

        if (result.length === 1) {
            this.response.send({item: result[0]});
        } else if (result.length > 1) {
            throw new ErrorModel('MULTIPLE_PROCESS_FOUND', 200);
        } else {
            throw new ErrorModel('PROCESS_NOT_FOUND', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /process/edit İşlem Düzenle
     * @apiName ProcessEdit
     * @apiGroup Process
     * @apiDescription  İşlemi Düzenler.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} processId İşlem ID'si
     * @apiParam {String} [categoryId] Kategori ID'si
     * @apiParam {String} [title] İşlem Başlığı
     * @apiParam {String} [processKey] İşlem Anahtarı
     * @apiParam {String=sql, file, request} [systemType] Sistem Tipi
     * @apiParam {Date} [expiredTime] Süre
     * @apiParam {Date} [hidden] Gizli Sorgu.
     * @apiParam {String} [dbKey] Veritabanı Anahtarı
     * @apiParam {String} [icon] İşlem İconu
     * @apiParam {Json} [presentParams] MpAnalysis hızlı erişim kodları
     * @apiParam {Json} [charts] Grafik Ayarları
     * @apiParam {Json} [queryItems] Sorgular
     * @apiParam {Json} [events] Sorgu Olayları
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": 'OK'
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiErrorExample {json} error Hata
     *     HTTP/1.1 4xxx error
     *     {
     *          error: SAVE_FIELDS_NOT_FOUND
     *          error: UPDATE_UNKNOWN_ERROR
     *          error: PROCESS_UPDATE_ERROR
     *     }
     *
     * @apiSampleRequest /process/edit
     */
    public async edit() {

        this.requireParameters(['processId']);

        const processId = this.getParam('id|processId');
        const process = this.getProcess();

        if (Object.keys(process).length === 0) {
            throw new ErrorModel('SAVE_FIELDS_NOT_FOUND', 200);
        }

        const result = await this.processDao.updateWithId(processId, {
            $set: process,
        });

        if (result.modifiedCount === 1) {
            this.response.send({success: 'OK'});
        } else {
            throw new ErrorModel('UPDATE_UNKNOWN_ERROR', 200);
        }
    }

    private getProcess() {
        return this.setValuesWithParams([
            'id|categoryId',
            'title',
            'processKey',
            'systemType',
            'expiredTime',
            'hidden',
            'dbKey',
            'icon',
            'queryItems',
            'events',
            'presentParams',
            'charts',
        ]).getValues();
    }
}
