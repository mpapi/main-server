/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import moment from 'moment';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {CategoryDao} from '../dao/CategoryDao';

import {BaseController} from './BaseController';

export class CategoryController extends BaseController {

    categoryDao: CategoryDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.categoryDao = new CategoryDao(this.db);
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /category/add Kategori Ekle
     * @apiName CategoryAdd
     * @apiGroup Category
     * @apiDescription Yeni Kategori
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} userId Kullanıcı ID'si
     * @apiParam {String} [parentId=null] Ana Kategori ID'si
     * @apiParam {String} title Kategori Adı
     * @apiParam {String} [order] Sıra
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /category/add
     */
    public async add() {

        this.requireParameters(['userId', 'title']);

        const insert = {
            userId: this.getParam('id|userId'),
            parentId: this.getParam('id|parentId') || null,
            title: this.getParam('title'),
            icon: this.getParam('icon') || '',
            order: this.getParam('order') || 999,
            deleted: false,
        };

        const result = await this.categoryDao.insert(insert);

        if (result.insertedCount === 1) {

            this.response.send({success: 'OK', insertedId: result.insertedId});
        } else {
            throw new ErrorModel('CATEGORY_NOT_BE_SAVED', 200);
        }

    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /category/edit Kategori Düzenle
     * @apiName CategoryEdit
     * @apiGroup Category
     * @apiDescription Kategori Düzenle
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} _id Kategori ID'si
     * @apiParam {String} [parentId] Ana Kategori ID'si
     * @apiParam {String} [title] Kategori Adı
     * @apiParam {String} [icon] İcon
     * @apiParam {String} [order] Sıra
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /category/edit
     */
    public async edit() {

        this.requireParameters(['_id']);

        const id = this.getParam('id|_id');
        const $set = this.setValues()
            .setValuesWithParams([
                'id|parentId',
                'title',
                'icon',
                'deleted',
                'order',
            ])
            .getValues();

        if (Object.keys($set).length === 0) {
            throw new ErrorModel('SAVE_FIELDS_NOT_FOUND', 200);
        }

        if (!!$set.deleted) {
            $set.deleted = {
                userId: this.user._id,
                datetime: moment().toDate(),
            };
        }

        const result = await this.categoryDao.updateWithId(id, {$set});

        if (result.modifiedCount === 1) {

            this.response.send({success: 'OK'});

        } else {
            throw new ErrorModel('CATEGORY_NOT_BE_SAVED', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /category/delete Kategori Sil
     * @apiName CategoryDelete
     * @apiGroup Category
     * @apiDescription Kategori Sil
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} _id Kategori ID'si
     * @apiParam {String} [userId=LogonUserId] Kullanıcı ID'si
     * @apiParam {String} [permanentlyDelete=false] Kalıcı olarak silinir. Root yetkisi gerektirir. (test aşamasında)
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK",
     *          "modifiedCount": 2
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /category/delete
     */
    public async delete() {

        this.requireParameters(['_id']);

        let userId = this.getParam('id|userId');
        if (userId == null) {
            userId = this.user._id;
        }

        const id = this.getParam('id|_id');

        // parent id ile alt kategorilerin tamamını alıyor.
        const items: any[] = await this.categoryDao.getIdList(userId, id);

        items.push(id);

        const result = await this.categoryDao.update({
            _id: {$in: items},
        }, {
            $set: {
                deleted: {
                    userId,
                    datetime: moment().toDate(),
                }
            },
        });

        this.response.send({
            success: 'OK',
            modifiedCount: result.modifiedCount,
        });
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /category/list Kategori Listesi
     * @apiName CategoryList
     * @apiGroup Category
     * @apiDescription Kategori Listesi
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} [userId=logonUserId] Kullanıcı ID'si yetki gerektirir.
     * @apiParam {Boolean} [showDeleted=false] Silinen Kategorileri getir.
     * @apiParam {Boolean} [flat=false] Kategorilerin düz liste mi yoksa ağaç olarak mı geleceğini belirtir.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /category/list
     */
    public async list() {

        let userId = this.user._id;
        if (this.isRoles(['root', 'admin']) && this.hasParam('userId')) {
            userId = this.getParam('id|userId');
        }

        const isFlat = this.getParam('flat') === true;
        const showDeleted = this.isRoles(['root', 'admin']) && this.getParam('showDeleted') === true;

        const docs = isFlat
            ? await this.categoryDao.getFlatList(userId, showDeleted)
            : await this.categoryDao.getList(userId, null, showDeleted);

        this.response.send({items: docs});
    }
}
