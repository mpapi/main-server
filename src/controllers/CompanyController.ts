/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import _ from 'lodash';
import moment from 'moment';
import {ObjectID} from 'mongodb';

import {Redis} from '../../core/src/connections/Redis';
import {ErrorModel} from '../../core/src/models/ErrorModel';
import {CompanyDao} from '../dao/CompanyDao';
import {UserDao} from '../dao/UserDao';
import {CompanyEntity} from '../entity/CompanyEntity';

import {BaseController} from './BaseController';

export class CompanyController extends BaseController {

    private client: Redis;
    private companyDao: CompanyDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.client = Redis.getInstance();
        this.companyDao = new CompanyDao(this.db);
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /company/add Firma Ekle
     * @apiName CompanyAdd
     * @apiGroup Company
     * @apiDescription Yeni Firma
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} title Firma Adı
     * @apiParam {String} [expiredTime=1 Month] Lisans Süresi tarih formatında alınır.
     * @apiParam {String} [﻿userLimit=1] Kullanıcı Limiti
     * @apiParam {String} [active=false] Firma Aktik Durumu
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiErrorExample {json} error Hata
     *     HTTP/1.1 4xxx error
     *     {
     *          error: ERROR_ADD_COMPANY_LIMIT
     *          error: ERROR_EXISTS_COMPANY
     *          error: COMPANY_NOT_BE_SAVED
     *          error: DATABASE_CONNECTION_ERROR
     *          error: SET_USER_ADMIN_COMPANY_NOT_BE_SAVED
     *          error: COMPANY_NOT_BE_SAVED
     *          error: INVALID_DATE_FORMAT
     *          error: OUT_OF_ALLOWED_DATE
     *          error: INSERT_ERROR
     *     }
     *
     * @apiSampleRequest /company/add
     */
    public async add() {

        this.requireParameters(['title']);

        const userId: ObjectID = this.user._id;
        const companyId: ObjectID = this.company._id;

        const mDate = this.getExpiredDate();
        if (_.isString(mDate)) {
            throw new ErrorModel(mDate, 200);
        }

        const companyItem = this
            .setValues({
                createdTime: moment().toDate(),
                expiredTime: mDate.toDate(),
                verified: false,
                added: {
                    userId,
                    datetime: moment().toDate(),
                },
                deleted: false,
                userLimit: 1,
                machineKey: '',
                active: false,
            })
            .setValuesWithParams([
                'title',
                'userLimit',
                'machineKey',
                'active'
            ])
            .getValues();

        const userDao = new UserDao(this.db);
        const company = await this.companyDao.getItem({title: companyItem.title});

        if (company) {
            throw new ErrorModel('ERROR_EXISTS_COMPANY', 200);
        }

        const result = await this.companyDao.insert(companyItem);

        if (result.insertedCount === 1) {
            await userDao.setAdminCompany(companyId, result.insertedId);

            // setAdminCompany ile şirket güncellendiği için cache temizlenmeli.
            await this.clearCache();

            this.response.send({success: 'OK', insertedId: result.insertedId});
        } else {
            throw new ErrorModel('COMPANY_NOT_BE_SAVED', 200);
        }

    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /company/edit Firma Düzenle
     * @apiName CompanyEdit
     * @apiGroup Company
     * @apiDescription Firma Düzenle
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} _id Firma Adı
     * @apiParam {String} [title] Firma Adı
     * @apiParam {String} [expiredTime=1 Month] Lisans Süresi
     * @apiParam {String} [﻿userLimit=1] Kullanıcı Limiti
     * @apiParam {String} [active=false] Firma Aktik Durumu
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": "OK"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /company/edit
     */
    public async edit() {

        this.requireParameters([
            ['_id', 'companyId']
        ]);

        const companyId = this.getParam('id|companyId') || this.getParam('id|_id');

        if (!this.isRoles(['root']) && companyId.toString() === this.user.companyId.toString()) {

            throw new ErrorModel('PERMISSION_ERROR', 200);
        }

        const $set = this.setValues()
            .setValuesWithParams([
                'title',
                'userLimit',
                'expiredTime',
                'machineKey',
                'active',
                'deleted',
            ])
            .setValuesWithParams([
                'verified',
            ], ['root'])
            .getValues();

        if (Object.keys($set).length === 0) {
            throw new ErrorModel('SAVE_FIELDS_NOT_FOUND', 200);
        }

        if (!!$set.deleted) {
            $set.deleted = {
                userId: this.user._id,
                datetime: moment().toDate(),
            };
        }

        const company = await this.companyDao.getItemWithId(companyId);

        if (!company) {
            throw new ErrorModel('COMPANY_NOT_FOUND', 200);
        }

        const result = await this.companyDao.updateWithId(companyId, {$set});

        if (result.modifiedCount === 1) {

            await this.clearCache(company);

            if (!!$set.deleted) {
                const userDao = new UserDao(this.db);
                await userDao.removeAdminCompany(companyId);

                // setAdminCompany ile şirket güncellendiği için cache temizlenmeli.
                await this.clearCache(company);
            }

            this.response.send({success: 'OK'});
        } else {
            throw new ErrorModel('UNKNOWN_ERROR', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /company/list Firma Listesi
     * @apiName CompanyList
     * @apiGroup Company
     * @apiDescription Firma Listesi
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "items": []
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /company/list
     */
    public async list() {
        // eğer root rolüne sahip ise filtre yoktur.
        const filter = this.isRoles(['root']) ? {} : {
            _id: {
                $ne: this.user.companyId, // kendi firmasını yönetemez
                $in: this.user.adminCompanies || [], // kendi eklemiş olduğu firmaları yönetebilir
            },
            deleted: false,
        };

        const result = await this.companyDao.getItems([
            {$match: filter},
            {$sort: {title: 1}},
        ]);

        this.response.send({items: result});
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /company/delete Firma Sil
     * @apiName CompanyDelete
     * @apiGroup Company
     * @apiDescription Firma Sil
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} companyId Firma ID
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "items": []
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /company/delete
     */
    public async delete() {
        this.parameters.deleted = true;
        await this.edit();
    }

    protected init() {
        this.companyDao = new CompanyDao(this.db);
    }

    private getExpiredDate(): moment.Moment | string {
        // gelen parametrenin date tipinde olacağını belirler.
        const expiredTime = this.getParam('date|expiredTime') || null;
        const mDate = expiredTime
            ? moment(expiredTime)
            : moment().add(1, 'month');

        if (!mDate.isValid()) {
            return 'INVALID_DATE_FORMAT';
        }

        return mDate
            .hours(0)
            .minutes(0)
            .seconds(0)
            .milliseconds(0);
    }

    clearCache(company?: CompanyEntity) {

        const _id = company ? company._id : this.company._id;
        const machineKey = company ? company.machineKey : this.company.machineKey;

        return this.client.clear([
            `company:${machineKey}`,
            `company:${_id}`
        ]);
    }
}
