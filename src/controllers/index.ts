import {CategoryController} from './CategoryController';
import {CompanyController} from './CompanyController';
import {DashboardController} from './mp-analysis/DashboardController';
import {ProcessController} from './ProcessController';
import {UserController} from './UserController';

/**
 * Created by hakan on 11/05/2019.
 */

export {CategoryController, CompanyController, DashboardController, ProcessController, UserController};
