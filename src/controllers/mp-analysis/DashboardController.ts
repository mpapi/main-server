/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';
import {ObjectID} from 'mongodb';

import {CategoryDao} from '../../dao/CategoryDao';
import {ProcessDao} from '../../dao/ProcessDao';
import {BaseController} from '../BaseController';

export class DashboardController extends BaseController {

    private categoryDao: CategoryDao;
    private processDao: ProcessDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.categoryDao = new CategoryDao(this.db);
        this.processDao = new ProcessDao(this.db);
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /mp-analysis/dashboard/status Dashboard Status
     * @apiName DashboardStatus
     * @apiGroup Dashboard
     * @apiDescription Dashboard status bilgilerini içerir.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          success: 'OK',
     *          totalCategory: 14,
     *          totalReport: 18
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiErrorExample {json} error Hata
     *     HTTP/1.1 4xxx error
     *     {
     *          error: ...
     *     }
     *
     * @apiSampleRequest /mp-analysis/dashboard/status
     */
    public async status() {

        const userId: ObjectID = this.user._id;

        const categoryIds = await this.categoryDao.getIdList(userId, false);
        const totalReport = await this.processDao.getListItems([
            {
                $match: {
                    categoryId: {$in: categoryIds},
                }
            }
        ]);

        this.response.send({
            success: 'OK',
            totalCategory: categoryIds.length,
            totalReport: totalReport.length
        });
    }

}
