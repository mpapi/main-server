/**
 * Created by hakan on 04/07/2017.
 */
import {ObjectID} from 'bson';
import {NextFunction, Response} from 'express';
import * as _ from 'lodash';
import moment from 'moment';
import uuid from 'uuid';

import {Redis} from '../../core/src/connections/Redis';
import {ErrorModel} from '../../core/src/models/ErrorModel';
import {CategoryDao} from '../dao/CategoryDao';
import {CompanyDao} from '../dao/CompanyDao';
import {ProcessDao} from '../dao/ProcessDao';
import {UserDao} from '../dao/UserDao';
import {Email} from '../emails/Email';
import {RegisterTemplate} from '../emails/templates';
import {CompanyEntity} from '../entity/CompanyEntity';
import {UserEntity} from '../entity/UserEntity';

import {BaseController} from './BaseController';

export class UserController extends BaseController {

    client: Redis;
    companyDao: CompanyDao;
    userDao: UserDao;
    categoryDao: CategoryDao;
    processDao: ProcessDao;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);

        this.client = Redis.getInstance();
        this.companyDao = new CompanyDao(this.db);
        this.userDao = new UserDao(this.db);
        this.categoryDao = new CategoryDao(this.db);
        this.processDao = new ProcessDao(this.db);
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/login Giriş
     * @apiName UserLogin
     * @apiGroup User
     * @apiDescription Sisteme Giriş Yapılır.
     *
     * @apiParam {String} [email] E-Posta
     * @apiParam {String} [username] Kullanıcı Adı
     * @apiParam {String} password Parola
     * @apiParam {Boolean} force Diğer oturumları kapatarak mevcut oturumu açar
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "token": "TOKEN STRING",
     *          "user": "UserEntity"
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/login
     */
    public async login() {

        // TODO * @apiParam {String} [platform=mp-api] Login servisinin kullanıldığı platform.

        this.requireParameters([['username', 'email'], 'password']);

        const filter = this.setValues()
            .setValuesWithParams([
                'username',
                'email',
            ])
            .getValues();

        const password = this.getParam('password');
        const force = this.getParam('force') || false;

        const user: UserEntity = await this.userDao.getItem(filter);

        if (!user) {
            throw new ErrorModel('USER_NOT_FOUND', 200);
        }

        const pass = this.getCryptoPassword(user._id, password);
        if (pass !== user.password) {
            throw new ErrorModel('PASSWORD_NOT_EQUALS', 200);
        }

        const isOpenSession: boolean = !!_.find(user.sessions, {
            logout: false,
            static: false
        } as any);

        if (!force && isOpenSession) {
            throw new ErrorModel('SESSION_IS_OPEN', 200);
        }

        // clear cache
        for (const session of user.sessions) {
            if (!session.static) {
                session.logout = true;
                await this.client.clear([`user:${session.token}`]);
            }
        }

        // clear logout sessions
        user.sessions = _.filter(user.sessions, (session) => !session.logout);

        // new token
        const tokenObj = this.getTokenObject();
        user.sessions.push({...tokenObj});

        const result = await this.userDao.updateWithId(user._id, {
            $set: {
                sessions: _.cloneDeep(user.sessions),
            }
        });

        // clear user
        // results will not be displayed in token.
        user.sessions = _.map(user.sessions, (session) => {
            delete session.token;
            return session;
        });
        delete user.password;

        if (result.modifiedCount === 1) {
            this.response.send({
                token: tokenObj.token,
                user,
            });
        } else {
            throw new ErrorModel('TOKEN_NOT_SAVE', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/logout Çıkış
     * @apiName UserLogout
     * @apiGroup User
     * @apiDescription Sistemden Çıkış Yapılır.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": true
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/logout
     */
    public async logout() {

        const token = this.getToken();

        const result = await this.userDao.updateWithId(this.user._id, {
            $pull: {
                sessions: {token}
            },
        });

        if (result.modifiedCount === 1) {

            await this.client.clear([
                `user:${token}`,
            ]);

            this.response.send({success: 'OK'});
        } else {
            throw new ErrorModel('DATA_NOT_BE_SAVED', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/auth Kullanıcı Doğrulama
     * @apiName UserAuth
     * @apiGroup User
     * @apiDescription Kullanıcı Girişini Doğrular ve kullanıcı bilgilerini alır.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiSuccess {String} user Kullanıcı Objesi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "user": {user object}
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/auth
     */
    public async auth() {

        /*
        Bu metoda gelebilmesi için, kullanıcının giriş yapmış olması gerekiyor. this.user ile giriş yapmış kullanıcıyı gönderebiliriz.
         */

        return this.response.send({user: new UserEntity(this.user)});
    }

    /**
     * @apiIgnore
     * @apiVersion 1.0.0
     * @api {post} /user/create Kayıt
     * @apiName UserCreate
     * @apiGroup User
     * @apiDescription Sisteme Yeni Kullanıcı Kaydı Yapılır.
     *
     * @apiParam {String} fullname Tam adı.
     * @apiParam {String} mail E-Posta Adresi.
     * @apiParam {String} username Kullanıcı Adı.
     * @apiParam {String} companyId Şirket ID'si.
     * @apiParam {String} isAdmin Kullanıcı admin mi? <Admin ve root> yetkilerinden birini gerektirir.
     * @apiParam {String} password Parola.
     * @apiParam {String} passwordRepeat Parola Tekrar.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": true
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/create
     */
    public async create() {

        this.requireParameters(['email', 'username', 'companyId', 'password', 'passwordRepeat']);

        const passVals = this
            .setValues({})
            .setValuesWithParams([
                'password',
                'passwordRepeat',
            ])
            .getValues();

        if (passVals.password !== passVals.passwordRepeat) {
            throw new ErrorModel('PASSWORDS_NOT_EQUALS', 200);
        }

        const newUserParams = this
            .setValues({
                authUserId: this.user._id,
            })
            .setValuesWithParams([
                'fullname',
                'email',
                'username',
                'password',
                'id|companyId',
            ])
            .getValues();

        const isAdmin: boolean = !!this.getParam('isAdmin');

        const user: UserEntity = this.getNewUser({}, newUserParams, {isAdmin});

        const isExists = await this.userDao.checkUsernameOrEmail(user.email, user.username);

        if (isExists) {
            throw new ErrorModel('USERNAME_OR_EMAIL_EXISTS', 200);
        }

        const result = await this.userDao.insert(user);

        if (result.insertedCount === 1) {
            const pass = this.getCryptoPassword(result.insertedId, passVals.password);

            // set password
            await this.userDao.updateWithId(result.insertedId, {$set: {password: pass}});
        } else {

            throw new ErrorModel('NEW_USER_DATA_NOT_BE_SAVED', 200);
        }

        await new Email({html: RegisterTemplate})
            .addTo(user.email)
            .setSubject('Hoş Geldiniz!')
            .addParam('FULLNAME', user.fullname || user.username)
            .addParam('EMAIL', user.email)
            .addParam('USERNAME', user.username)
            .addParam('PASSWORD', passVals.password)
            .send();

        this.response.send({success: 'OK'});
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/copy Kopyala
     * @apiName UserCopy
     * @apiGroup User
     * @apiDescription Sistemde var olan kullanıcının tüm özellikleriyle ve yeni bir kullanıcı adıyla kopyasını oluşturur.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} copyUserId Kopyası oluşturulacak kullanıcının ID'si
     * @apiParam {String} companyId Firma ID'si
     * @apiParam {String} isAdmin Kullanıcı admin mi? <Admin ve root> yetkilerinden birini gerektirir.
     * @apiParam {String} [fullname] Tam Adı
     * @apiParam {String} email E-Posta Adresi
     * @apiParam {String} username Kullanıcı Adı
     * @apiParam {String} password Parola
     * @apiParam {String} passwordRepeat Parola Tekrar
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": true
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/copy
     */
    public async copy() {

        this.requireParameters(['copyUserId', 'email', 'username', 'companyId', 'password', 'passwordRepeat']);

        const companyId = this.getParam('id|companyId');

        const company: CompanyEntity = await this.companyDao.getItemWithId(companyId);

        // total users
        const totalUser: any[] = await this.userDao.getUsers(this.user._id, this.isRoles(['root']));

        // company users
        const companyUser: any[] = await this.userDao.getItems([
            {$match: {companyId}}
        ]);

        // Giriş yapmış kullanıcının şirketine ait toplam kullanıcı sayısı, bu şirkete ait kullanıcı limitinin
        // üzerine çıkamaz.
        if ((this.company.userLimit - totalUser.length) <= 0) {
            throw new ErrorModel('MAX_NUMBER_OF_TOTAL_USER', 200);
        }

        // mevcut şirketin kullanıcı sayısı, kullanıcı limitinden fazla olamaz.
        if ((company.userLimit - companyUser.length) <= 0) {
            throw new ErrorModel('MAX_NUMBER_OF_COMPANY_USER', 200);
        }

        const password = this.getParam('password');
        const passwordRepeat = this.getParam('passwordRepeat');
        if (password !== passwordRepeat) {
            throw new ErrorModel('PASSWORDS_NOT_EQUALS', 200);
        }

        const newUserParams: UserEntity = this
            .setValues({
                companyId
            })
            .setValuesWithParams([
                'email',
                'fullname',
                'username',
                'authUserId',
            ])
            .getValues();

        const copyUserId = this.getParam('id|copyUserId');
        const isAdmin = !!this.getParam('isAdmin');
        const copyTasks = !!this.getParam('copyTasks');

        const isExists = await this.userDao.checkUsernameOrEmail(newUserParams.email, newUserParams.username);

        if (isExists) {
            throw new ErrorModel('USERNAME_OR_EMAIL_EXISTS', 200);
        }

        const oldUserParams = await this.userDao.getCopyUser(copyUserId);

        const user = this.getNewUser(oldUserParams, newUserParams, {isAdmin, copyTasks});

        const result = await this.userDao.insert(user);
        if (result.insertedCount === 1) {

            const updateResult = await this.userDao.updateWithId(result.insertedId, {
                $set: {password: this.getCryptoPassword(result.insertedId, password)},
            });

            if (updateResult.modifiedCount !== 1) {
                throw new ErrorModel('PASSWORD_NOT_BE_SAVED', 200);
            }
        } else {
            throw new ErrorModel('NEW_USER_DATA_NOT_BE_SAVED', 200);
        }

        const categories = await this.categoryDao.getList(copyUserId, null);

        const {loopError, processList} = await this.recursiveCopy(result.insertedId, null, categories);

        if (loopError) {
            throw new ErrorModel('CATEGORY_COPY_ERROR', 200, loopError);
        }

        if (processList && processList.length > 0) {
            await this.processDao.inserts(processList);
        }

        await new Email({html: RegisterTemplate})
            .addTo(user.email)
            .setSubject('Hoş Geldiniz!')
            .addParam('FULLNAME', user.fullname || user.username)
            .addParam('EMAIL', user.email)
            .addParam('USERNAME', user.username)
            .addParam('PASSWORD', password)
            .send();

        this.response.send({success: 'OK'});
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/list Liste
     * @apiName UserList
     * @apiGroup User
     * @apiDescription  Kullanıcıların Listesini Verir.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "items": []
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/list
     */
    public async list() {

        const result = await this.userDao.getUsers(this.user._id, this.isRoles(['root']));

        this.response.send({items: result});
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/tasks Görev Listesi
     * @apiName UserTasks
     * @apiGroup User
     * @apiDescription Kullanıcıların Listesini Verir.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} [userId] Kullanıcıya ait tüm görevleri listeler.
     * @apiParam {String} [companyId] Firmaya ait tüm görevleri listeler.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "items": []
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/tasks
     */
    public async tasks() {
        const values = this
            .setValues({
                userId: this.user && this.user._id,
                companyId: this.company && this.company._id
            })
            .getValues();

        const result = await this.userDao.getTasks(values, this.isRoles(['root']));

        this.response.send({items: result});
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/edit Düzenle
     * @apiName UserEdit
     * @apiGroup User
     * @apiDescription  Kullanıcı Düzenler.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} [userId=LogonUserId] Kullanıcı ID'si
     * @apiParam {String} companyId
     * @apiParam {String} fullname
     * @apiParam {String} email
     * @apiParam {String} username
     * @apiParam {String} password
     * @apiParam {String} passwordRepeat
     * @apiParam {String} expiredTime
     * @apiParam {Boolean} verified
     * @apiParam {Boolean} active
     * @apiParam {Boolean} deleted
     * @apiParam {Json} sessions
     * @apiParam {Json} permission
     * @apiParam {JsonArray} details
     * @apiParam {Json} defaultValues
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": 'OK'
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/edit
     */
    public async edit() {
        let userId = this.getParam('id|userId');
        if (userId == null) {
            userId = this.user._id;
        }

        const $edited = this
            .setValuesWithParams([
                'id|companyId',
                'fullname',
                'email',
                'username',
                'expiredTime',
                '[?]|adminCompanies',
                'verified',
                'active',
                'dbList',
                'permission',
                'details',
                'defaultValues',
                'defaultServerName',
                'dashboard',
                'tasks',
            ])
            .setValuesWithParams([
                'active',
            ], [
                'root',
                'admin'
            ])
            .setValuesWithParams([
                'verified',
                'roles',
            ], [
                'root'
            ])
            .getValues();

        const user = await this.userDao.getItemWithId(userId);

        if (!user) {
            throw new ErrorModel('USER_NOT_FOUND', 200);
        }

        if (this.hasParam('password') && this.hasParam('passwordRepeat')) {
            const p1 = this.getParam('password');
            const p2 = this.getParam('passwordRepeat');

            if (p1 === p2) {
                $edited.password = this.getCryptoPassword(userId, p1);
            } else {
                throw new ErrorModel('PASSWORDS_NOT_EQUALS', 200);
            }
        }

        if (this.hasParam('deleted') && this.getParam('deleted') === true) {
            $edited.deleted = {
                by: this.user._id,
                datetime: moment().toDate(),
            };
        }

        if (Object.keys($edited).length === 0) {
            throw new ErrorModel('SAVE_FIELDS_NOT_FOUND', 200);
        }

        const result = await this.userDao.updateWithId(userId, {
            $set: $edited,
        });

        if (result.modifiedCount === 1) {

            // clear cache
            for (const session of user.sessions) {
                await this.client.clear(`user:${session.token}`);
            }

            this.response.send({success: 'OK'});
        } else {
            throw new ErrorModel('UPDATE_UNKNOWN_ERROR', 200);
        }
    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /user/delete Sil
     * @apiName UserDelete
     * @apiGroup User
     * @apiDescription  Kullanıcı Düzenler.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} userId Kullanıcı ID'si
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": 'OK'
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /user/delete
     */
    public async delete() {

        this.requireParameters(['userId']);

        const userId = this.getParam('id|userId');

        const categoryIds: any[] = await this.categoryDao.getIdList(userId);

        const items = await this.processDao.getItems([{
            $match: {
                categoryIds,
                filter: {categoryId: {$in: categoryIds}},
            }
        }]);

        const processIds: any[] = [];
        for (let i = 0; i < (items || []).length; i++) {
            processIds.push(items[i]._id);
        }

        await this.processDao.delete({
            _id: {$in: processIds},

        });

        await this.categoryDao.delete({
            _id: {$in: categoryIds},
        });

        await this.userDao.delete({_id: userId});

        this.response.send({success: 'OK'});
    }

    async recursiveCopy(newUserId: ObjectID, parentId: ObjectID | null, categories: any[]) {
        const processList: any[] = [];
        let loopError;

        const count = categories.length;

        for (let i = 0; i < count; i++) {
            const category = categories[i];
            const oldCategoryId = category._id;
            const children = category.children;

            delete category._id;
            delete category.children;

            category.parentId = parentId;
            category.userId = newUserId;

            await this.categoryDao.insert(category)
                .then((categoryResult) => {
                    return this.processDao.getItems([{
                        $match: {
                            categoryId: oldCategoryId,
                        },
                    }]).then((processes) => {
                        return {
                            processes: processes || [],
                            categoryId: categoryResult.insertedId,
                        };
                    });
                })
                .then(({processes, categoryId}) => {

                    for (const process of processes) {
                        delete process._id;
                        process.categoryId = categoryId;
                    }

                    processList.push(...processes);
                    return categoryId;
                })
                .then((categoryId) => this.recursiveCopy(newUserId, categoryId, children))
                .then((items) => processList.push(...items.processList))
                .catch((copyCategoryError) => loopError = copyCategoryError);
        }

        return {
            loopError,
            processList,
        };
    }

    private getNewUser(
        oldUserParams: any,
        newUserParams: any = {},
        opts: { isAdmin?: boolean, copyTasks?: boolean } = {isAdmin: false, copyTasks: false}
    ): UserEntity {
        return new UserEntity({
            dbList: [],
            sessions: [],
            verified: false,
            active: true,
            lastPasswords: [],
            dashboard: [],
            tasks: [],
            createdTime: moment().toDate(),
            added: {
                userId: newUserParams.authUserId,
                datetime: moment().toDate(),
            },
            permissions: {
                users: {
                    add: true,
                    copy: true,
                    edit: true,
                    delete: true
                },
                companies: {
                    add: true,
                    edit: true,
                    delete: true
                },
                mpAnalysis: {
                    showTasks: true,
                    changePassword: true,
                    changeDefaultEndpoint: true
                }
            },
            ...oldUserParams,
            ...newUserParams,
            ...(opts.isAdmin ? {
                adminCompanies: [
                    newUserParams.companyId
                ],
                roles: _.sortedUniq(['admin', ...oldUserParams.roles])
            } : {
                adminCompanies: [],
                roles: ['user']
            }),
            ...(opts.copyTasks ? {tasks: oldUserParams.tasks} : {})
        });
    }

    private getTokenObject() {
        return {
            createdTime: moment().toDate(),
            agent: this.request.headers['user-agent'],
            token: uuid.v1(),
            logout: false,
            static: false,
        };
    }
}
