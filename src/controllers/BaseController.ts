/**
 * Created by hakan on 04/07/2017.
 */
import {NextFunction, Response} from 'express';

import {HttpController} from '../../core/src/controllers/HttpController';
import {CompanyEntity} from '../entity/CompanyEntity';

export class BaseController extends HttpController {

    company: CompanyEntity;

    constructor(req: any, res: Response, next: NextFunction) {
        super(req, res, next);
        this.company = req.company;
    }

}
