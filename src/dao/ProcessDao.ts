/**
 * Created by hakan on 04/07/2017.
 */

import {Db} from 'mongodb';

import {MongoCoreDao} from '../../core/src/dao/MongoCoreDao';

export class ProcessDao extends MongoCoreDao {

    public constructor(db: Db) {
        super(db, 'processes');
    }

    public getListItems(aggregateFilter?: any[], customProject?: object) {
        const aggregate = [
            {
                $lookup: {
                    from: 'categories',
                    localField: 'categoryId',
                    foreignField: '_id',
                    as: 'category',
                },
            },
            {$unwind: '$category'},
            {
                $project: {
                    title: true,
                    categoryId: true,
                    categoryTitle: '$category.title',
                    processKey: true,
                    expiredTime: true,
                    hidden: true,
                    dbKey: true,
                    icon: true,
                    events: true,
                    ...(customProject || {}),
                },
            },
            {$sort: {title: 1}}
        ];
        if (aggregateFilter) {
            aggregateFilter = [...aggregate, ...aggregateFilter];
        }
        return super.getItems(aggregateFilter);
    }
}
