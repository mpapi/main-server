/**
 * Created by hakan on 04/07/2017.
 */

import {Db, ObjectID} from 'mongodb';

import {MongoCoreDao} from '../../core/src/dao/MongoCoreDao';

export class CategoryDao extends MongoCoreDao {

    public constructor(db: Db) {
        super(db, 'categories');
    }

    public getFlatList(userId: ObjectID, showDeleted?: boolean) {

        const query: any = {
            userId,
        };

        if (showDeleted === false) {
            query.deleted = false;
        }

        return this.collection
            .find(query)
            .sort({order: 1})
            .toArray();
    }

    public getList(userId: ObjectID, parentId: ObjectID | null, showDeleted?: boolean): Promise<any[]> {

        return new Promise(async (resolve, reject) => {
            try {
                const data = await this.getSubCategories(userId, parentId, showDeleted);
                resolve(data);
            } catch (e) {
                reject(e);
            }
        });
    }

    public getIdList(userId: ObjectID, showDeleted?: boolean): Promise<any[]> {

        return new Promise(async (resolve, reject) => {
            try {
                const data = await this.getFlatList(userId, showDeleted);
                const ids: any[] = this.getIds(data);
                resolve(ids);
            } catch (e) {
                reject(e);
            }
        });
    }

    private async getSubCategories(userId: ObjectID, parentId: ObjectID | null, showDeleted?: boolean) {

        const query: any = {
            userId,
            parentId,
        };

        if (showDeleted === false) {
            query.deleted = false;
        }

        const result = await this.collection
            .aggregate([
                {$match: query},
                {
                    $lookup: {
                        from: 'processes',
                        localField: '_id',
                        foreignField: 'categoryId',
                        as: 'processes',
                    },
                },
                {$project: {userId: 0, parentId: 0}},
                {
                    $project: {
                        title: 1,
                        icon: 1,
                        order: 1,
                        deleted: 1,
                        processCount: {$size: '$processes'},
                    },
                },
                {$sort: {order: 1}},
            ])
            .toArray();

        for (const item of result) {
            item.children = await this.getSubCategories(userId, item._id, showDeleted);
        }

        return result;
    }

    private getIds(data: Array<{ _id: any, children: any[] }>): any[] {
        const ids: any = [];

        for (const item of data) {
            ids.push(item._id);
        }
        return ids;
    }
}
