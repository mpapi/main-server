/**
 * Created by hakan on 04/07/2017.
 */

import {Db} from 'mongodb';

import {MongoCoreDao} from '../../core/src/dao/MongoCoreDao';

export class CompanyDao extends MongoCoreDao {
    public constructor(db: Db) {
        super(db, 'companies');
    }
}
