/**
 * Created by hakan on 04/07/2017.
 */

import * as _ from 'lodash';
import {Db, ObjectID, UpdateWriteOpResult} from 'mongodb';

import {MongoCoreDao} from '../../core/src/dao/MongoCoreDao';
import {ErrorModel} from '../../core/src/models/ErrorModel';

export class UserDao extends MongoCoreDao {

    public constructor(db: Db) {
        super(db, 'users');
    }

    public checkUsernameOrEmail(email: string, username: string, opts?: object) {

        return this.collection.findOne({
            $or: [
                {email},
                {username},
            ],
        }, opts).then((doc) => {
            return doc !== null;
        }, (err) => {
            return err;
        });
    }

    public async setAdminCompany(adminCompanyId: ObjectID, companyId: ObjectID) {

        return this.update({
            companyId: adminCompanyId,
            roles: 'admin'
        }, {
            $push: {
                adminCompanies: companyId,
            },
        });
    }

    public removeAdminCompany(companyId: ObjectID): PromiseLike<UpdateWriteOpResult> | PromiseLike<null> {

        return this.update({
            adminCompanies: companyId
        }, {
            $pull: {
                adminCompanies: companyId
            },
        });
    }

    public async getCopyUser(copyUserId: ObjectID) {

        const copyUser = await this.getItem({
            _id: copyUserId,
        }, {
            $project: {
                _id: false,
                companyId: true,
                expiredTime: true,
                servers: true,
                dbList: true,
                defaults: true,
                roles: true,
                permission: true,
                dashboard: true,
                tasks: true,
                active: true,
                verified: true, // TODO ödeme sistemi eklenince burası kaldırılacak.
            }
        });

        return copyUser;
    }

    public getItem(filter: any, opts?: object): Promise<any | null> {

        return new Promise(async (resolve, reject) => {

            const aggregateFilter: any[] = [
                {$match: filter},
                {
                    $lookup: {
                        from: 'companies',
                        localField: 'companyId',
                        foreignField: '_id',
                        as: 'company',
                    },
                },
                {$unwind: '$company'}
            ];

            if (opts) {
                aggregateFilter.push(opts);
            }

            this.collection.aggregate(aggregateFilter).toArray((error, result) => {
                if (error) {
                    reject(new ErrorModel('USER_LIST_NOT_READING', 200, error));
                } else {
                    if (_.isArray(result) && result.length > 0) {
                        resolve(result[0]);
                    } else {
                        resolve();
                    }
                }
            });
        });
    }

    public getTasks({userId, companyId}: { userId?: ObjectID, companyId?: ObjectID }, isRootUser: boolean) {
        return new Promise(async (resolve, reject) => {

            const aggregateFilter = [
                {$match: {'tasks.active': true, '$or': [{_id: userId}, {companyId}]}},
                {$unwind: {path: '$tasks', preserveNullAndEmptyArrays: false}},
                {$addFields: {'tasks.userId': '$_id'}},
                {$project: {_id: false, tasks: true}},
                {$replaceRoot: {newRoot: '$tasks'}},
                {$sort: {title: 1}}
            ];

            if (isRootUser) {
                aggregateFilter.shift();
            }

            this.collection.aggregate(aggregateFilter).toArray((error, result) => {
                if (error) {
                    reject(new ErrorModel('USER_TASK_LIST_NOT_READING', 200, error));
                } else {
                    resolve(result);
                }
            });
        });
    }

    public getUsers(logonUserId: ObjectID, isRootUser: boolean) {

        return new Promise<any[]>(async (resolve, reject) => {

            const aggregateFilter = [
                {$match: {_id: logonUserId}},
                {$unwind: {path: '$adminCompanies', preserveNullAndEmptyArrays: true}},
                {
                    $lookup: {
                        from: 'users',
                        localField: 'adminCompanies',
                        foreignField: 'companyId',
                        as: 'companyUsers',
                    },
                },
                {$unwind: {path: '$companyUsers', preserveNullAndEmptyArrays: true}},
                {
                    $match: {
                        'companyUsers._id': {$ne: logonUserId},
                        'companyUsers': {$ne: null},
                        ...(
                            isRootUser ? {} : {'companyUsers.roles': {$eq: 'user'}}
                        )
                    }
                },
                {
                    $project: {
                        _id: false,
                        companyUser: '$companyUsers',
                    },
                },
                {$group: {_id: '$companyUser'}},
                {$replaceRoot: {newRoot: '$_id'}},
                {
                    $lookup: {
                        from: 'companies',
                        localField: 'companyId',
                        foreignField: '_id',
                        as: 'company',
                    },
                },
                {$unwind: {path: '$company', preserveNullAndEmptyArrays: true}},
                {
                    $project: {
                        'sessions.token': false,
                        'password': false,
                    },
                },
                {$sort: {username: 1}},
            ];

            if (isRootUser) {
                aggregateFilter.shift();
            }

            this.collection.aggregate(aggregateFilter).toArray((error, result) => {
                if (error) {
                    reject(new ErrorModel('USER_LIST_NOT_READING', 200, error));
                } else {
                    resolve(result);
                }
            });
        });
    }
}
