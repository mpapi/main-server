/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../core/src/entity/routes/Method';
import {accessRoles} from '../../core/src/middleware';
import {CoreRoutes} from '../../core/src/routers/CoreRoutes';
import {CategoryController} from '../controllers';
import {auth} from '../middleware/auth';

export class CategoryRoute {

    public static create() {

        const authLayer = auth();
        const rolesLayer = accessRoles(['root', 'admin']);

        return CoreRoutes.routes(
            CategoryController,
            {
                fnName: 'add',
                method: Method.post,
                middleware: [authLayer, rolesLayer],
                routePath: '/category/add'
            },
            {
                fnName: 'edit',
                method: Method.post,
                middleware: [authLayer, rolesLayer],
                routePath: '/category/edit'
            },
            {
                fnName: 'delete',
                method: Method.post,
                middleware: [authLayer, rolesLayer],
                routePath: '/category/delete'
            },
            {
                fnName: 'list',
                method: Method.post,
                middleware: [authLayer],
                routePath: '/category/list'
            },
        );
    }

}
