/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../core/src/entity/routes/Method';
import {accessRoles} from '../../core/src/middleware';
import {CoreRoutes} from '../../core/src/routers/CoreRoutes';
import {CompanyController} from '../controllers';
import {auth} from '../middleware/auth';

export class CompanyRoute {

    public static create() {
        const authLayer = auth();
        const rolesLayer = accessRoles(['root', 'admin']);

        return CoreRoutes.routes(
            CompanyController,
            {
                fnName: 'add',
                routePath: '/company/add',
                method: Method.post,
                middleware: [authLayer, rolesLayer]
            },
            {
                fnName: 'edit',
                routePath: '/company/edit',
                method: Method.post,
                middleware: [authLayer, rolesLayer, auth('companies.edit', true)]
            },
            {
                fnName: 'list',
                routePath: '/company/list',
                method: Method.post,
                middleware: [authLayer, rolesLayer]
            },
            {
                fnName: 'delete',
                routePath: '/company/delete',
                method: Method.post,
                middleware: [authLayer, rolesLayer, auth('companies.delete', true)]
            },
        );
    }

}
