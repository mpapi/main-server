/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../../core/src/entity/routes/Method';
import {CoreRoutes} from '../../../core/src/routers/CoreRoutes';
import {DashboardController} from '../../controllers';
import {auth} from '../../middleware/auth';

export class DashboardRoute {

    public static create() {

        const authLayer = auth();

        return CoreRoutes.routes(
            DashboardController,
            {
                fnName: 'status',
                routePath: '/mp-analysis/dashboard/status',
                middleware: [authLayer],
                method: Method.post
            }
        );

    }

}
