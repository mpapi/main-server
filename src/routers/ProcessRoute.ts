/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../core/src/entity/routes/Method';
import {accessRoles} from '../../core/src/middleware';
import {CoreRoutes} from '../../core/src/routers/CoreRoutes';
import {ProcessController} from '../controllers';
import {auth} from '../middleware/auth';

export class ProcessRoute {

    public static create() {

        const authLayer = auth();
        const rolesLayer = accessRoles(['root', 'admin']);

        return CoreRoutes.routes(
            ProcessController,
            {
                fnName: 'add',
                routePath: '/process/add',
                middleware: [authLayer, rolesLayer],
                method: Method.post
            },
            {
                fnName: 'list',
                routePath: '/process/list',
                middleware: [authLayer],
                method: Method.post
            },
            {
                fnName: 'get',
                routePath: '/process/get',
                middleware: [authLayer],
                method: Method.post
            },
            {
                fnName: 'edit',
                routePath: '/process/edit',
                middleware: [authLayer, rolesLayer],
                method: Method.post
            },
            {
                fnName: 'delete',
                routePath: '/process/delete',
                middleware: [authLayer, rolesLayer],
                method: Method.post
            },
        );

    }

}
