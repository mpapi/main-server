/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../core/src/entity/routes/Method';
import {accessRoles} from '../../core/src/middleware';
import {CoreRoutes} from '../../core/src/routers/CoreRoutes';
import {UserController} from '../controllers';
import {auth} from '../middleware/auth';

export class UserRoute {

    public static create() {
        const authLayer = auth();
        const rolesLayer = accessRoles(['root', 'admin']);

        return CoreRoutes.routes(
            UserController,
            {
                fnName: 'login',
                routePath: '/user/login',
                method: Method.post
            },
            {
                fnName: 'logout',
                routePath: '/user/logout',
                middleware: [authLayer],
                method: Method.post
            },
            {
                fnName: 'auth',
                routePath: '/user/auth',
                middleware: [authLayer],
                method: Method.post
            },
            {
                fnName: 'create',
                routePath: '/user/create',
                middleware: [authLayer, rolesLayer, auth('users.add', true)],
                method: Method.post
            },
            {
                fnName: 'copy',
                routePath: '/user/copy',
                middleware: [authLayer, rolesLayer, auth('users.copy', true)],
                method: Method.post
            },
            {
                fnName: 'tasks',
                routePath: '/user/tasks',
                middleware: [authLayer],
                method: Method.post
            },
            {
                fnName: 'list',
                routePath: '/user/list',
                middleware: [authLayer, rolesLayer],
                method: Method.post
            },
            {
                fnName: 'edit',
                routePath: '/user/edit',
                middleware: [authLayer, rolesLayer, auth('users.edit', true)],
                method: Method.post
            },
            {
                fnName: 'delete',
                routePath: '/user/delete',
                middleware: [authLayer, rolesLayer, auth('users.delete', true)],
                method: Method.post
            },
        );
    }
}
