/**
 * Created by hakan on 11/05/2019.
 */
import {CategoryRoute} from './CategoryRoute';
import {CompanyRoute} from './CompanyRoute';
import {ProcessRoute} from './ProcessRoute';
import {UserRoute} from './UserRoute';

export default [
    CategoryRoute.create(),
    CompanyRoute.create(),
    ProcessRoute.create(),
    UserRoute.create(),
];
//         router.use('', );
//         router.use('/user',);
//
//         router.use('', MpAnalysis.register());
//
//         return router;
//     }
// }
