/**
 * Created by hakan on 04/07/2017.
 */
import {MongoDB} from '../core/src/connections/MongoDB';
import {Redis} from '../core/src/connections/Redis';
import {DatabaseModel} from '../core/src/models/DatabaseModel';
import {ErrorModel} from '../core/src/models/ErrorModel';
import Application from '../core/src/server';
import {Config} from '../core/src/utils/Config';

import Routers from './routers';
import Tasks from './tasks';

const database = Config.get('database');
const dbConf = new DatabaseModel(database);

const redisUrl = Config.get('redisUrl');

class App extends Application {

    async injectRequest(): Promise<{ [key: string]: any }> {
        const injects: { [key: string]: any } = {};

        try {
            injects.db = await MongoDB.connect(dbConf);
        } catch (e) {
            throw new ErrorModel('DATABASE_CONNECTION_ERROR', 200, e);
        }

        return injects;
    }

    async beforeConfigHooks(): Promise<void> {
        if (redisUrl) {
            try {
                Redis.useRedis(redisUrl);
            } catch (e) {
                throw new ErrorModel('REDIS_CONNECTION_ERROR', 200, e);
            }
        }
    }

    async afterConfigHooks() {
        // time based or cron like
        // schedule tasks
        Tasks.register();
    }

    async registerRoutes(): Promise<any[]> {
        return Routers;
    }
}

(async () => {

    const app = new App();

    await app.listen();

})();
