import {NextFunction, Response} from 'express';
import * as _ from 'lodash';
import {Db} from 'mongodb';

import {MongoDB} from '../../core/src/connections/MongoDB';
import {Redis} from '../../core/src/connections/Redis';
import {HttpController} from '../../core/src/controllers/HttpController';
import {DatabaseModel} from '../../core/src/models/DatabaseModel';
import {ErrorModel} from '../../core/src/models/ErrorModel';
import {Config} from '../../core/src/utils/Config';
import {CompanyDao} from '../dao/CompanyDao';
import {UserDao} from '../dao/UserDao';
import {CompanyEntity} from '../entity/CompanyEntity';
import {UserEntity} from '../entity/UserEntity';

const database = Config.get('database');

const checkPermission = (user: UserEntity, perm?: string, eq?: any): void => {
    if (!!perm) {
        const isRoot = HttpController.isRole(user, 'root');
        const isAccess = _.result(user, `permission.${perm}`) === eq;

        if (!!perm && !isRoot && !isAccess) {
            throw new ErrorModel('ACCESS_DENIED', 200);
        }
    }
};

const checkUser = (user: UserEntity): void => {
    if (!user) {
        throw new ErrorModel('SESSION_NOT_FOUND', 200);
    }

    if (!user.active) {
        throw new ErrorModel('USER_IS_NOT_ACTIVE', 200);
    }

    if (!user.verified) {
        throw new ErrorModel('USER_IS_NOT_VERIFIED', 200);
    }
};

const checkCompany = (company: CompanyEntity): void => {
    if (!company) {
        throw new ErrorModel('COMPANY_NOT_FOUND', 200);
    }

    if (!company.active) {
        throw new ErrorModel('COMPANY_IS_NOT_ACTIVE', 200);
    }

    if (!company.verified) {
        throw new ErrorModel('COMPANY_IS_NOT_VERIFIED', 200);
    }
};

const getModels = async () => {
    const dbCfg = new DatabaseModel(database);
    const db: Db = await MongoDB.connect(dbCfg);

    const userDao = new UserDao(db);
    const companyDao = new CompanyDao(db);

    return {userDao, companyDao};
};

export const auth = (perm?: string, eq?: any) => async (req: any, res: Response, next: NextFunction) => {

    const client = Redis.getInstance();

    const token = req.headers.token;
    const machineKey = req.headers['machine-key'];

    if (!token && !machineKey) {
        res.send({error: 'TOKEN_OR_ACCESS_KEY_NOT_FOUND'});
    }

    // öncelik token'da :)

    try {

        if (token) {

            const cacheUser = await client.get(`user:${token}`, [
                '_id',
                'companyId',
                'adminCompanies',
                'added.userId',
                'company._id',
                'company.added.userId'
            ]);

            if (cacheUser) {

                req.user = cacheUser;

            } else {
                const {userDao} = await getModels();

                const user: UserEntity = await userDao.getItem({
                    sessions: {$elemMatch: {token, logout: false}}
                }, {
                    $project: {
                        'sessions.token': false,
                        'password': false,
                    },
                });

                checkUser(user);

                await client.set(`user:${token}`, user);

                req.user = user;
            }

            const cacheCompany = cacheUser && await client.get(`company:${cacheUser.companyId}`, [
                '_id',
                'added.userId'
            ]);

            if (cacheCompany) {

                req.company = cacheCompany;

            } else {
                const {companyDao} = await getModels();

                const company = await companyDao.getItemWithId(req.user.companyId);

                checkCompany(company);

                await client.set(`company:${company._id}`, company);

                req.company = company;
            }

            checkPermission(cacheUser, perm, eq);

        } else { // if (machineKey)

            const cacheCompany = await client.get(`company:${machineKey}`, [
                '_id',
            ]);

            req.user = {roles: ['machine']};

            if (cacheCompany) {

                req.company = cacheCompany;

            } else {

                const {companyDao} = await getModels();

                const company: CompanyEntity = await companyDao.getItem({machineKey});

                checkCompany(company);

                req.company = company;
            }
        }

        next();

    } catch (error) {
        console.error(error);
        res.send(new ErrorModel('AUTH_ERROR', 200, error));
    }
};
