import {CoreEmail} from '../../core/src/emails/CoreEmail';
import {Config} from '../../core/src/utils/Config';

const email: IEmailConfig = Config.get('email');

export class Email extends CoreEmail {

    constructor(template: IEmailTemplate) {
        super(email, template);
    }

}
