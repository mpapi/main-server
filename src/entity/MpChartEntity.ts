export class MpChartEntity {
    key?: string;
    title?: string;
    subtitle?: string;
    icon?: string;
    default?: boolean;

    // x sütununun hangi veri alanından alacağını belirtir.
    xColumnName?: string;

    // bir grafik içerisine birden fazla farklı şekiller eklenebilir.
    series?: Array<{
        type?: string;
        text?: string;
        columnName?: string;
    }>;
}
