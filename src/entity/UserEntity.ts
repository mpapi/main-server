import {ObjectID} from 'mongodb';

import {ActionEntity} from './ActionEntity';
import {DbEntity} from './DbEntity';
import {DefaultEntity} from './DefaultEntity';
import {MpDashboardEntity} from './MpDashboardEntity';
import {PermissionEntity} from './PermissionEntity';
import {TaskEntity} from './TaskEntity';
import {UserDetailEntity} from './UserDetailEntity';

export class UserEntity {

    constructor(vals?: any) {
        this._id = vals._id;
        this.companyId = vals.companyId;
        this.fullname = vals.fullname;
        this.email = vals.email;
        this.username = vals.username;
        this.password = vals.password;
        this.details = vals.details || [];
        this.expiredTime = vals.expiredTime;
        this.verified = vals.verified || false;
        this.active = vals.active || false;
        this.added = vals.added;
        this.adminCompanies = vals.adminCompanies || [];
        this.dbList = vals.dbList || [];
        this.defaults = vals.defaults || [];
        this.tasks = vals.tasks || [];
        this.sessions = vals.sessions || [];
        this.roles = vals.roles || [];
        this.permission = vals.permission || {};
        this.deleted = vals.deleted || false;

        this.dashboard = vals.dashboard;
    }

    _id: ObjectID;
    companyId: ObjectID;
    fullname: string;
    email: string;
    username: string;
    password: string;
    details: UserDetailEntity[];
    expiredTime: string;
    verified: boolean;
    active: boolean;
    added: ActionEntity;
    adminCompanies: string[];
    dbList: DbEntity[];
    defaults: DefaultEntity[];
    sessions: any[];
    roles: string[];
    tasks?: Array<TaskEntity<any>>;
    permission: PermissionEntity;
    deleted: ActionEntity | false;

    // mp-analysis
    dashboard: MpDashboardEntity[];
}
