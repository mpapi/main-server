import {ObjectID} from 'bson';

export class CompanyEntity {
    _id!: ObjectID;
    title!: string;
    createdTime?: string;
    expiredTime?: string;
    userLimit!: number;
    verified?: boolean;
    active?: boolean;
    added?: {
        userId?: string,
        datetime?: string;
    };
    machineKey?: string;
    deleted?: boolean;
}
