const assert = require('chai').assert;
const request = require('request');
const config = require('../../config.tests.json');

describe('/list', function () {
    this.timeout(15 * 1000);
    it('Category List Request Success', function (done) {
        config.options.url = '/category/list';
        request(config.options, function (error, response, body) {
            if (error) {
                done('error: ' + error.message);
                return;
            }
            assert.isObject(body, 'Json Formatında Değil!');

            assert.isDefined(body.items, 'items tanımlı değil');
            assert.isArray(body.items, 'items array değil');
            done();
        });
    });

    it('Category List Request Success width userId', function (done) {
        config.options.url = '/category/list';
        config.options.json.userId = '5b0a870c86be0f88e3d360b4';
        request(config.options, function (error, response, body) {
            if (error) {
                done('error: ' + error.message);
                return;
            }
            assert.isObject(body, 'Json Formatında Değil!');

            assert.isDefined(body.items, 'items tanımlı değil');
            assert.isArray(body.items, 'items array değil');
            done();
        });
    });
});
