const assert = require('chai').assert;
const request = require('request');
const config = require('../../config.tests.json');

describe('/list', function () {
    this.timeout(15 * 1000);
    it('Company List Request Success', function (done) {
        config.options.url = '/company/list';
        request(config.options, function (error, response, body) {
            if (error) {
                done('error: ' + error.message);
                return;
            }
            assert.isObject(body, 'Json Formatında Değil!');

            assert.isDefined(body['items'], 'items tanımlı değil');
            assert.isArray(body['items'], 'items array değil');
            done();
        });
    });
});
