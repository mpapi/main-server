const assert = require('chai').assert;
const request = require('request');
const _ = require('lodash');
const config = require('../../config.tests.json');

describe('/login && /logout', function () {
    this.timeout(15 * 1000);
    let token;

    it('User Login Request Success', function (done) {
        const opts = _.cloneDeep(config.options);
        opts.url = '/user/login';
        opts.json = {
            username: 'root',
            password: 'hkn',
        };

        request(opts, function (error, response, body) {
            if (error) {
                done('error: ' + error.message);
                return;
            }

            assert.isObject(body, 'Json Formatında Değil!');
            assert.isDefined(body['token'], 'token tanımlı değil');
            token = body['token'];
            assert.isDefined(body['user'], 'user tanımlı değil');

            done();
        });
    });
    it('User Logout Request Success', function (done) {
        assert.isDefined(token, 'Giriş testi başarılı çalışmadı!');

        const opts = _.cloneDeep(config.options);
        opts.url = '/user/logout';
        opts.headers.token = token;

        request(opts, function (error, response, body) {
            if (error) {
                done('error: ' + error.message);
                return;
            }

            assert.isObject(body, 'Json Formatında Değil!');
            assert.isDefined(body['success'], 'success tanımlı değil');
            done();
        });
    });
});
