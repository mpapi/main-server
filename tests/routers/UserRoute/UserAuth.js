const assert = require('chai').assert;
const request = require('request');
const config = require('../../config.tests.json');

describe('/auth', function () {
    this.timeout(15 * 1000);
    it('User Auth Request Success', function (done) {
        config.options.url = '/user/auth';
        request(config.options, function (error, response, body) {
            if (error) {
                done('error: ' + error.message);
                return;
            }

            assert.isObject(body, 'Json Formatında Değil!');
            assert.isDefined(body['user'], 'user tanımlı değil');

            const sessions = body['user']['sessions'];
            assert.isArray(sessions, 'oturum bilgileri alınıyor');
            for (let i = 0; i < sessions.length; i++) {
                assert.isUndefined(sessions[i]['token'], 'Oturum Bilgilerinde token olmamalı');
            }

            done();
        });
    });
});
