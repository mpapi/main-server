import {check, fail, sleep} from 'k6';
import http from 'k6/http';

// const baseURL = "http://localhost:3000";
const baseURL = 'https://main-server.hakanketen.com';
export const options = {
    vus: 20,
    duration: '30s',
    // stages: [
    //     {duration: "1m", target: 10},
    //     {duration: "1m", target: 20},
    //     {duration: "1m", target: 5}
    // ],
    // thresholds: {
    //     http_req_duration: ["avg<100", "p(95)<200"]
    // },
    userAgent: 'K6UserAgent/1.0'
};

export default function () {

    const url = `${baseURL}/user/login`;

    const data = {
        username: 'root',
        password: 'hkn',
        force: true
    };

    const res = http.post(
        url, JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json'
            }
        }
    );

    return check(res, {'login succeeded': () => !res.body.error}) || fail('login failed');
}
