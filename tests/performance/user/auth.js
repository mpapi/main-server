import {check, fail} from 'k6';
import http from 'k6/http';

// const baseURL = "http://localhost:3000";
const baseURL = 'https://main-server.hakanketen.com';
export const options = {
    vus: 20,
    duration: '30s',
    // stages: [
    //     {duration: "1m", target: 10},
    //     {duration: "1m", target: 20},
    //     {duration: "1m", target: 5}
    // ],
    // thresholds: {
    //     http_req_duration: ["avg<100", "p(95)<200"]
    // },
    userAgent: 'K6UserAgent/1.0'
};

export default function () {

    const url = `${baseURL}/user/auth`;

    const data = {
        // username: 'root',
        // password: 'hkn',
        // force: true
    };

    const res = http.post(
        url, JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
                'token': '86e33a50-b2a4-11e9-a724-6dd55e8e78c5'
            }
        }
    );

    return check(res, {'auth succeeded': () => !res.body.error && !res.body.user}) || fail('auth failed');
}
